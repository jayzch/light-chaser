import {AbstractDesignerLoader} from "./AbstractDesignerLoader";

/**
 * 展示模式下的设计器加载器
 */
export class ViewDesignerLoader extends AbstractDesignerLoader {
    protected loadProjectData(): void {
    }

    protected scanComponents(): void {
    }

}